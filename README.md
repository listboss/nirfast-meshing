# Meshing Tools
Set of utilities that helps user to create meshes for nirfast software (http://code.google.com/p/nirfast/)

(This package is mainly `developed` and maintained by  Hamid R. Ghadyani from Dartmouth College)

**nirfast** is an FEM-based software package for modeling near infrared (NIR) frequency domain light transport in tissue.
To model the tissue in **nirfast**, this project provides necessary tools for researchers to convert their clinical images (from MR, CT, XRay...) to a mathematical model suitable for nirfast.
